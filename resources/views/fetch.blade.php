<!--Sinan Samet-->


@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/fetch.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="container white-bg">
        <!-- Show welcome message -->
        <h2 class="panel-body">
            Overview Events
        </h2>

        <form>
            <div class="form-group row">
                <label for="smFormGroupInput" class="col-sm-2 col-form-label col-form-label-sm">Events off</label>
                <div class="col-sm-10">
                    <select id="events-by-person" class="form-control form-control-sm" id="smFormGroupInput">
                        <option value="all">Show all</option>
                        @foreach($people as $person)
                            <option value="{{$person->id}}">{{$person->voornaam}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>

        <table class="table">
            <thead>
            <tr>
                <th>Number</th>
                <th>Event</th>
                <th>Date</th>
                <th>Person</th>
            </tr>
            </thead>
            <tbody id="events-content">
            @foreach($events as $event)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$event->titel}}</td>
                    <td>{{$event->datum}}</td>
                    <td>{{$event->voornaam}} {{$event->achternaam}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('js/fetch.js') }}"></script>
@endsection
