<!--Sinan Samet Babete Zengers  -->
<!--Put the standard layout here for each page -->
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('layouts.head')
</head>
<body>
    @include('layouts.bubbles')
    <div id="app">
        @include('layouts.nav')
        @yield('content')
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
    </script>
    @yield('javascript')
</body>
</html>