@extends('layouts.app')


@section('content')
    <!-- Show the welcome user message on the home page -->
    <h2 class="panel-body">
        Welcome {{ Auth::user()->voornaam }} {{ Auth::user()->achternaam }}
    </h2>

    <h3>Do you know the members of our team? You can prove it by matching the names and images!</h3>
    <br>
    <div id="placeholders">
    </div>
    <br>
    <br>
    <div id="seizeImg">
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('js/dragAndDrop.js') }}"></script>
@endsection