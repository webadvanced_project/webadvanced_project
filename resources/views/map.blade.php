<!-- Yannick -->
@extends('layouts.app')
@section('content')
    <title>Event Map</title>
    <div id='wrap'>
    <div id="map"></div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('js/map.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyD6yCDn66xAmlTd6bNeAponDRU1XggxZEw&callback=initMap') }}"></script>
@endsection