<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EventsController@index');

//All events
Route::get('/events/', 'EventsController@index');

//Find event on id
Route::get('/events/{id}', 'EventsController@getByEventId');

//Find event on person
Route::get('/events/person/{id}', 'EventsController@getByPersonId');

//Find event on date
Route::get('/events/from/{from}/to/{to}', 'EventsController@getEventsFromTo');

//Find event on person and date
Route::get('/person/{id}/events/from/{from}/to/{to}', 'EventsController@getEventsFromPersonFromTo');

Auth::routes();

Route::get('/home','HomeController@index');

Route::get('/logout','Auth\LoginController@logout');

Route::get('/aanmakenevenement','EvenementController@aanmaken');

Route::get('/map', 'MapController@index');

//Route to EventController
Route::get('/events','EventsController@events');

Route::post('/add-event','EventsController@addEvent');

//Javascript fetch
Route::get('/fetch','EventsController@fetch');