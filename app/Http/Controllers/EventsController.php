<?php
//Sinan Samet & Babette Zengers
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use App\Person;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

class EventsController extends Controller
{
    function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return DB::table('events')->join('people', 'events.persoon_id', '=', 'people.id')->get();
    }

    public function getByEventId($id){
        return Events::find($id);
    }

    public function getByPersonId($id){
        return DB::table('events')->join('people', 'events.persoon_id', '=', 'people.id')->where('persoon_id', $id)->get();
    }

    public function getEventsFromTo($from, $to){
        return DB::table('events')->whereBetween('datum', [$from, $to])->get();
    }

    public function getEventsFromPersonFromTo($id, $from, $to){
        return DB::table('events')->where('persoon_id', $id)->whereBetween('datum', [$from, $to])->get();
    }
    //Get data for calendar, only for logged in user
    public function events()
    {

        $events = Events::all()->where('persoon_id', '=', Auth::id());
        return view('events', compact('events'));
        //Compact-> variable pushed to the view
    }

    //Add event function for AJAX request
    public function addEvent(Request $request)
    {
        try{
            //POST variables
            $date = str_limit($request->date, 24, '');
            $date = Carbon::createFromFormat('D M d Y h:i:s', $date)->toDateTimeString();
            $title = $request->title;

            //Make model instance
            $events = new Events();

            //Put variable in colums
            $events->titel = $title;
            $events->datum = $date;
            $events->persoon_id = Auth::id();

            //Save in database
            $events->save();

            return "Event has been successfully added!";
        }
        catch (\Exception $e){
            return Auth::id() . " Event has not been uploaded: " . $e;
        }
    }

    //Fetch javascript
    public function fetch(){
        $person = new Person();
        $people = $person::all();
        $events = DB::table('events')->join('people', 'events.persoon_id', '=', 'people.id')->get();
        return view('fetch', compact('people', 'events'));
    }
}
