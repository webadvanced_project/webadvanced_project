<?php
// Parameters
    function index(){
      $sql = "SELECT * FROM events";
      return fetchData($sql);
    }
    function getByEventId($id){
      $sql = "SELECT * FROM events WHERE Id = '$id'";
      return fetchData($sql);
    }
    function getByPersonId($id){
      $sql = "SELECT * FROM events WHERE persoon_id = '$id'";
      return fetchData($sql);
    }
    function getEventsFromTo($from, $to){
      $sql = "SELECT * FROM events WHERE datum BETWEEN '$from' AND '$to' ";
      return fetchData($sql);
    }
    function getEventsFromPersonFromTo($id, $from, $to){
      $sql = "SELECT * FROM events WHERE persoon_id = '$id' AND datum BETWEEN '$from' AND '$to' ";
      return fetchData($sql);
    }
    function getConnection($dbname){

    $config = file_get_contents('config.json');
    $config = json_decode($config);

     try {
         $conn = new PDO("mysql:host=$config->servername;dbname=$dbname", $config->username, $config->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
      } catch (Exception $e) {
        return "Connection failed";
      }
        
    }
    function fetchData($sql){

      if (isset($sql)) {
          // PDO
          try {
              $conn = getConnection("monkeybusiness");
              $stmt = $conn->prepare($sql);
              $stmt->execute();
              $jsonData = array();

              foreach($stmt->fetchAll() as $k=>$v) {
                  $jsonData[] = $v;
              }
              return json_encode($jsonData);

              $conn = null;
          } catch(PDOException $e) {
              return "Connection failed: " . $e->getMessage();
          }
      }
    }


?>
