/**
 * Created by Sinan, Nick on 5/20/2017.
 */
function testDiv(ev, x) {
    if (x.length > 0) {
        return false;
    }
    else {
        allowDrop(ev);
        return true;
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

//Put images in div and checkt if they are at the right position
function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));

    checkCompletion();
}

function checkCompletion(){
    var complete = true;
    var finished = true;
    $('.dnd').each(function(index){
        var dndBoxName = $(this).data('name');
        var dndImgName = $(this).find('img').data('name');
        if(dndBoxName !== dndImgName){
            complete = false;
        }
        if(dndImgName === undefined){
            finished = false
        }
    });

    if(finished === true && complete === false){
        alert("Sorry not wrong, try again!");
        init();
    }
    else if(complete === true){
        alert("Good job! You got it right!");
    }
}

function init(){
    //Reset
    $('#placeholders').html('');
    $('#seizeImg').html('');

    //Populate placeholders and images
    var people = ['Thomas','Nick','Sinan','Yannick','Babette'];
    var peopleDiffOrder = ['Babette', 'Thomas','Nick','Sinan','Yannick'];
    $.each(people, function(key, val){
        $('#placeholders').append('<div class="dndBox">' +
            '<div class="label">'+val+'</div><div class="dnd" data-name="'+val+'" ondrop="drop(event)"' +
            'ondragover="testDiv(event, innerHTML)"></div></div>');

        $('#seizeImg').append('<img id="'+peopleDiffOrder[key]+'" data-name="'+peopleDiffOrder[key]+'" src="img/'+peopleDiffOrder[key]+'.jpg" draggable="true" ondragstart="drag(event)"'+
            'ondrop="drop(event)" height="186">');

    });
}

$(function() {
    init();

});