/**
 * Yannick Van Herck
 */
var map, infoWindow;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 7
    });
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {

            // Create a list of locations along with their long and lat coörds.
            var eventLocations = [
                ['Hasselt', 50.93069, 5.33248, 4],
                ['Luik', 50.6325574, 5.5796662, 5],
                ['Brussel', 50.8503463, 4.3517211, 3],
                ['Oostende', 51.21543, 2.928656, 2],
                ['Maastricht', 50.8513682, 5.6909725, 1]
            ];

            // Set a position(current position according to the location provided by your browser).
            // You must give permission to use your current location when asked!
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            // Create a marker for your current position.
            var marker = new google.maps.Marker({
                position: pos,
                title:"You are here (hopefully...)"
            });

            // Create several markers for different locations where events are taking place
            // Change icon & colour of event markers
            var pinColor = "0080FF";
            var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                new google.maps.Size(21, 34),
                new google.maps.Point(0,0),
                new google.maps.Point(10, 34));
            var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                new google.maps.Size(40, 37),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 35));

            // Create the event markers and add them to the map via a for-loop.
            var i, eventMarker;
            for (i = 0; i < eventLocations.length; i++) {
                eventMarker = new google.maps.Marker({
                    position: new google.maps.LatLng(eventLocations[i][1], eventLocations[i][2]),
                    map: map,
                    icon: pinImage,
                    shadow: pinShadow
                });

                // Create an eventlistener so the location's name appears when clicking on an event marker
                google.maps.event.addListener(eventMarker, 'click', (function(eventMarker, i) {
                    return function() {
                        infoWindow.setContent(eventLocations[i][0]);
                        infoWindow.open(map, eventMarker);
                    }
                })(eventMarker, i));
            }

            map.setCenter(pos);
            marker.setMap(map);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}
