/**
 * Created by Sinan on 5/20/2017.
 */
$(function() {
    $('#events-by-person').on('change', function () {
        var id = $(this).val();
        var url = "/events/person/"+id;
        if(id === 'all'){
            url = "/"
        }
        $.ajax({
            method: "GET",
            url: url
        }).done(function (result) {
            var content = $('#events-content');
            content.html('');

            $.each(result, function (key, val) {
                content.append(
                    '<tr>' +
                        '<th scope="row">'+(key+1)+'</th>' +
                        '<td>'+val.titel+'</td>' +
                        '<td>'+val.datum+'</td>' +
                        '<td>'+val.voornaam+' '+val.achternaam+'</td>' +
                    '</tr>'
                )
            })
        });
    })
});